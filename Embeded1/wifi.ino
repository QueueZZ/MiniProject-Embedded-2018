void wifi_init() {
  Serial.printf("Connecting to %s ", ssid);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
  }
  Serial.println(" connected");

  Serial.write(0x11);
  Serial.write(2);
  delay(1000);
  WiFiClient client;
  if (client.connect(host, 80))
  {
    String get = "GET /api";
    client.print(get + " HTTP/1.1\r\n" +
                 "Host: " + host + "\r\n" +
                 "Connection: close\r\n" +
                 "\r\n"
                );
    client.stop();
  }
}

void send_post(uint8_t buff[655]){
  WiFiClient client;
  if (client.connect(host, 80))
  {
    String img = (String)buff;
    String post = "POST /api/img?img=" + img;
    client.print(post + " HTTP/1.1\r\n" +
                 "Host: " + host + "\r\n" +
                 "Connection: close\r\n" +
                 "\r\n"
                );
    client.stop();
  }
}

